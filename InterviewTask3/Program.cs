﻿using InterviewTask3.Comparers;
using InterviewTask3.Models;
using System;

namespace InterviewTask3
{
    internal class Program
    {
        private static int Main(string[] args)
        {
            var areaComparer = new AreaComparer();
            var result1 = areaComparer.Compare(new Square(a: 80.0), new Cylinder(radius: 20.0, height: 10.0));
            Console.WriteLine("Square(a: 80.0) is{0}equals to Cylinder(radius: 20.0, height: 10.0)", result1 == 0 ? " " : " not ");
            var result2 = areaComparer.Compare(new Cube(a: 30.0), new Triangle(a: 13.0, height: 60.0));
            Console.WriteLine("Cube(a: 30.0) is{0}equals to Triangle(a: 13.0, height: 60.0)", result2 == 0 ? " " : " not ");
            var volumeComparer = new VolumeComparer();
            var result3 = volumeComparer.Compare(new Cube(a: 30.0), new Cylinder(radius: 20.0, height: 10.0));
            Console.WriteLine("Cube(a: 30.0) is{0}equals to Cylinder(radius: 20.0, height: 10.0)", result3 == 0 ? " " : " not ");

            return 0;
        }
    }
}