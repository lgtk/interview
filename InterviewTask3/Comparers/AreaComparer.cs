﻿using InterviewTask3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewTask3.Comparers
{
    public class AreaComparer : IComparer<Shape>
    {
        public int Compare(Shape x, Shape y)
        {
            return x.CalculateArea().CompareTo(y.CalculateArea());
        }
    }
}