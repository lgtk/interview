﻿using InterviewTask3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewTask3.Comparers
{
    public class VolumeComparer : IComparer<Solid>
    {
        public int Compare(Solid x, Solid y)
        {
            return x.CalculateVolume().CompareTo(y.CalculateVolume());
        }
    }
}