﻿namespace InterviewTask3.Models
{
    public class Square : Shape
    {
        public Square(double a)
        {
            A = a;
        }

        public double A { get; set; }

        public override double CalculateArea()
        {
            return 4 * A;
        }
    }
}