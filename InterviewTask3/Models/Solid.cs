﻿namespace InterviewTask3.Models
{
    public abstract class Solid : Shape
    {
        public abstract double CalculateVolume();
    }
}