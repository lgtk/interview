﻿using System;

namespace InterviewTask3.Models
{
    public class Cylinder : Solid
    {
        public double Radius { get; init; }

        public double Height { get; init; }

        public Cylinder(double radius, double height)
        {
            Radius = radius;
            Height = height;
        }

        public override double CalculateArea()
        {
            return 2 * Math.PI * Radius * Height + 2 * Math.PI * Math.Pow(Radius, 2);
        }

        public override double CalculateVolume()
        {
            return Math.PI * Math.Pow(Radius, 2) * Height;
        }
    }
}