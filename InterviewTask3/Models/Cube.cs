﻿using System;

namespace InterviewTask3.Models
{
    public class Cube : Solid
    {
        public double A { get; init; }

        public Cube(double a)
        {
            A = a;
        }

        public override double CalculateArea()
        {
            return 6 * Math.Pow(A, 2);
        }

        public override double CalculateVolume()
        {
            return Math.Pow(A, 3);
        }
    }
}