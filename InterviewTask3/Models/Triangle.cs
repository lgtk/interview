﻿namespace InterviewTask3.Models
{
    public class Triangle : Shape
    {
        public Triangle(double a, double height)
        {
            A = a;
            Height = height;
        }

        public double A { get; }
        public double Height { get; }

        public override double CalculateArea()
        {
            return A * Height / 2;
        }
    }
}