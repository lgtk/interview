﻿using System;

namespace InterviewTask3.Models
{
    public class Circle : Shape
    {
        public double Radius { get; init; }

        public Circle(double radius)
        {
            Radius = radius;
        }

        public override double CalculateArea()
        {
            return 2 * Math.PI * Radius;
        }
    }
}