﻿namespace InterviewTask3.Models
{
    public abstract class Shape
    {
        public abstract double CalculateArea();
    }
}