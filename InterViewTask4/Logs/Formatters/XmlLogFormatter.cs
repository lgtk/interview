﻿using System.IO;
using System.Xml.Serialization;

namespace InterViewTask4.Logs.Formatters
{
    public class XmlLogFormatter : ILogFormatter
    {
        public string Format(LogEntry logEntry)
        {
            var serializer = new XmlSerializer(typeof(LogEntry));
            using var textWriter = new StringWriter();
            serializer.Serialize(textWriter, logEntry);
            return textWriter.ToString();
        }
    }
}