﻿using System.Text.Json;

namespace InterViewTask4.Logs.Formatters
{
    public class JSonLogFormatter : ILogFormatter
    {
        public string Format(LogEntry logEntry)
        {
            return JsonSerializer.Serialize(logEntry);
        }
    }
}