﻿namespace InterViewTask4.Logs.Formatters
{
    public class PlainTextLogFormatter : ILogFormatter
    {
        public string Format(LogEntry logEntry)
        {
            return $"{logEntry.Timestamp.ToString("D")}, [{string.Join(",", logEntry.Tags)}] : {logEntry.Message}]";
        }
    }
}