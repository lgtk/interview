﻿namespace InterViewTask4.Logs.Formatters
{
    public interface ILogFormatter
    {
        string Format(LogEntry logEntry);
    }
}