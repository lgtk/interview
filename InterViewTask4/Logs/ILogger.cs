﻿namespace InterViewTask4.Logs
{
    public interface ILogger
    {
        void Log(LogEntry logEntry);
    }
}