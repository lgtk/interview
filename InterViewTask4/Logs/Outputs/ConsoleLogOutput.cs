﻿using InterViewTask4.Logs.Formatters;
using System;

namespace InterViewTask4.Logs.Outputs
{
    public class ConsoleLogOutput : ILogOutput
    {
        private readonly ILogFormatter formatter;

        public ConsoleLogOutput(ILogFormatter formatter)
        {
            this.formatter = formatter;
        }

        public void Save(LogEntry logEntry)
        {
            Console.WriteLine(formatter.Format(logEntry));
        }
    }
}