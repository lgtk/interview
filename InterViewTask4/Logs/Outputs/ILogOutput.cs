﻿namespace InterViewTask4.Logs.Outputs
{
    public interface ILogOutput
    {
        void Save(LogEntry logEntry);
    }
}