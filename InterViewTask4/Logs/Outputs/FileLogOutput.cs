﻿using InterViewTask4.Logs.Formatters;
using System.IO;

namespace InterViewTask4.Logs.Outputs
{
    public class FileLogOutput : ILogOutput
    {
        private readonly string filePath;
        private readonly ILogFormatter formatter;

        public FileLogOutput(string filePath, ILogFormatter formatter)
        {
            this.filePath = filePath;
            this.formatter = formatter;
        }

        public void Save(LogEntry logEntry)
        {
            using var fileWriter = new StreamWriter(filePath, true);
            fileWriter.WriteLine(formatter.Format(logEntry));
        }
    }
}