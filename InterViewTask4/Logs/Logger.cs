﻿using InterViewTask4.Logs.Outputs;

namespace InterViewTask4.Logs
{
    public class Logger : ILogger
    {
        private readonly ILogOutput _logOutput;

        public Logger(ILogOutput logOutput)
        {
            _logOutput = logOutput;
        }

        public void Log(LogEntry logEntry)
        {
            _logOutput.Save(logEntry);
        }
    }
}