﻿using InterViewTask4.Logs;
using InterViewTask4.Logs.Formatters;
using InterViewTask4.Logs.Outputs;
using System;
using System.Linq;

namespace InterViewTask4
{
    public static class Tester4
    {
        public static void Test()
        {
            var formatters = new ILogFormatter[] { new PlainTextLogFormatter(), new XmlLogFormatter(), new JSonLogFormatter() };
            //TODO: Each log type should have a separate file
            var outputs = formatters.SelectMany(formatter => new ILogOutput[] { new FileLogOutput("file1.log", formatter), new ConsoleLogOutput(formatter) });
            var loggers = outputs.Select(output => new Logger(output));

            foreach (var logger in loggers)
            {
                logger.Log(new LogEntry() { Message = "test", Tags = new[] { "t1", "t2" }, Timestamp = DateTime.UtcNow });
            }
        }
    }
}