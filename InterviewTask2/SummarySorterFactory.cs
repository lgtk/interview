﻿using System;

namespace InterviewTask2
{
    public static class SummarySorterFactory
    {
        public static ISummarySorter Create(SortOptions sortOptions)
        {
            ISummarySorter result;
            switch (sortOptions)
            {
                case SortOptions.Alphabetical:
                    result = new CharSummarySorter();
                    break;

                case SortOptions.ByCount:
                    result = new CountSummarySorter();
                    break;

                default:
                    throw new InvalidOperationException();
            }
            return result;
        }
    }
}