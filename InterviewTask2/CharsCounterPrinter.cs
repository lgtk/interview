﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewTask2
{
    public class CharsCounterPrinter
    {
        private readonly CharsCounter charsCounter;
        private readonly ISummarySorter sorter;
        private readonly string[] availableExtensions = new string[] { ".log", ".txt" };

        public CharsCounterPrinter(CharsCounter charsCounter, SortOptions sortOptions)
        {
            this.charsCounter = charsCounter;
            sorter = SummarySorterFactory.Create(sortOptions);
        }

        public void DisplayFileSummary(string filePath)
        {
            ValidatePath(filePath);
            ValidateFileType(filePath);
            var fileSummary = sorter.Sort(charsCounter.ReadAndCalculateFile(filePath));
            foreach (var singleCharSummary in fileSummary.CharsSummary)
            {
                Console.WriteLine($"{singleCharSummary.Key} -  {singleCharSummary.Value} / {CalculatePercents(fileSummary.TotalChars, singleCharSummary.Value)}%");
            }
        }

        private int CalculatePercents(int totalCount, int charCount)
        {
            return (int)Math.Round((double)(100 * charCount) / totalCount);
        }

        private void ValidatePath(string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException($"File not exist, path: {path}");
        }

        private void ValidateFileType(string path)
        {
            var ext = Path.GetExtension(path);
            if (!availableExtensions.Any(availableExt => availableExt == ext)) throw new FormatException($"File with type {ext} is not supported");
        }
    }
}