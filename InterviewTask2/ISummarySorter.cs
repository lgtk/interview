﻿namespace InterviewTask2
{
    public interface ISummarySorter
    {
        OutputSummary Sort(OutputSummary outputSummary);
    }
}