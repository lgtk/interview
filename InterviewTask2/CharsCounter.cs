﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace InterviewTask2
{
    public class CharsCounter
    {
        public OutputSummary ReadAndCalculateFile(string path)
        {
            return CountChars(path);
        }

        private OutputSummary CountChars(string path)
        {
            var charsCounter = new Dictionary<char, int>();
            using var reader = new StreamReader(path);
            var count = 0;
            char readedChar;
            while (reader.Peek() >= 0)
            {
                readedChar = (char)reader.Read();

                if (charsCounter.ContainsKey(readedChar))
                {
                    charsCounter[readedChar]++;
                }
                else
                {
                    charsCounter.Add(readedChar, 1);
                }
                count++;
            }

            return new OutputSummary(charsCounter, count);
        }
    }
}