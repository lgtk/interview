﻿using System;

namespace InterviewTask2
{
    internal class Program
    {
        private static int Main(string[] args)
        {
            //TODO: IOC, TESTS, CONOLE ARGS
            var presenter = new CharsCounterPrinter(new CharsCounter(), SortOptions.Alphabetical);
            presenter.DisplayFileSummary("test_file.txt");
            return 0;
        }
    }
}