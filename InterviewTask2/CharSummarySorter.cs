﻿using System.Linq;

namespace InterviewTask2
{
    public class CharSummarySorter : ISummarySorter
    {
        public OutputSummary Sort(OutputSummary outputSummary)
        {
            return new OutputSummary(outputSummary.CharsSummary.OrderBy(c => c.Key).ToDictionary(x => x.Key, x => x.Value), outputSummary.TotalChars);
        }
    }
}