﻿using System.Linq;

namespace InterviewTask2
{
    public class CountSummarySorter : ISummarySorter
    {
        public OutputSummary Sort(OutputSummary outputSummary)
        {
            return new OutputSummary(outputSummary.CharsSummary.OrderBy(c => c.Value).ToDictionary(x => x.Key, x => x.Value), outputSummary.TotalChars);
        }
    }
}