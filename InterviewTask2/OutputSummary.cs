﻿using System.Collections.Generic;

namespace InterviewTask2
{
    public record OutputSummary(Dictionary<char, int> CharsSummary, int TotalChars);
}