﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Interview;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview.Tests
{
    [TestClass()]
    public class Task1Tests
    {
        [TestMethod()]
        public void RewriteTableTest()
        {
            var table = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            var rewritedTable = Task1.RewriteTable(table);

            Assert.IsTrue(rewritedTable.Count == 3);

            var expectedValue = new int[] { 8, 5, 2 };
            for (int i = 0; i < rewritedTable.Count; i++)
            {
                Assert.AreEqual(rewritedTable[i], expectedValue[i]);
            }
        }

        [TestMethod()]
        public void RecalculateTableTest()
        {
            var table = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
            var rewritedTable = Task1.RecalculateTable(table);

            Assert.AreEqual(rewritedTable.Count, table.Length);
        }

        [TestMethod()]
        public void MultiplyElementsTest()
        {
            var table = new int[] { 0, 91, 100 };
            var rewritedTable = Task1.MultiplyElements(table);

            var expected = new[] { (1, 2), (2, 2) };

            var first = rewritedTable.First();
            var firstExpected = expected.First();

            Assert.IsTrue(first.Item1 == firstExpected.Item1 && first.Item2 == firstExpected.Item2);

            var second = rewritedTable.Last();
            var secondExpected = expected.Last();

            Assert.IsTrue(second.Item1 == secondExpected.Item1 && second.Item2 == secondExpected.Item2);
        }

        [TestMethod()]
        public void ChangeElementsLocationTest()
        {
            var table = new int[] { 3, 2, 1, 2, 3, 4, 5, 0 };
            var rewritedTable = Task1.ChangeElementsLocation(table, 1);

            var expectedTable = new int[] { 2, 3, 1, 2, 3, 4, 5, 0 };

            for (int i = 0; i < expectedTable.Length; i++)
            {
                Assert.AreEqual(expectedTable[i], rewritedTable[i]);
            }
        }

        [TestMethod()]
        public void CalculateCountOfGrowingSquencesTest()
        {
            var table = new int[] { 3, 2, 2, 1, 3, 4, 5, 0 };
            var (countOfGrowingSequences, sequences) = Task1.CalculateCountOfGrowingSquences(table);

            var expectedCountOfSequences = 2;
            Assert.AreEqual(expectedCountOfSequences, countOfGrowingSequences);

            var expectedSequences = new List<int[]> { new[] { 1, 3, 4 }, new[] { 3, 4, 5 } };

            Assert.IsTrue(Enumerable.SequenceEqual(sequences.First(), expectedSequences.First()));

            Assert.IsTrue(Enumerable.SequenceEqual(sequences.Last(), expectedSequences.Last()));
        }
    }
}