﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interview
{
    public static class Task1
    {
        private const int TABLE_SIZE = 200;

        /// <summary>
        /// AD 1
        /// </summary>
        /// <param name="tableSize"></param>
        /// <returns></returns>
        public static int[] CreateTableWithSize(int tableSize = TABLE_SIZE)
        {
            var random = new Random();
            var table = new int[tableSize];
            for (int i = 0; i < table.Length; i++)
            {
                table[i] = random.Next(0, 100);
            }
            return table;
        }

        /// <summary>
        /// AD 2
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static IList<int> RewriteTable(int[] table)
        {
            var result = new List<int>();
            for (int index = table.Count() - 1; index >= 0; index -= 3)
            {
                result.Add(table[index]);
            }
            return result;
        }

        /// <summary>
        /// AD 3
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static IList<double> RecalculateTable(int[] table)
        {
            var result = new List<double>() { CalculateAverage(50, table[1]) };
            for (int i = 1; i < table.Length - 1; i++)
            {
                result.Add(CalculateAverage(table[i - 1], table[i + 1]));
            }
            result.Add(CalculateAverage(table.Last(), 50));
            return result;
        }

        private static double CalculateAverage(int first, int second) => first + second / 2;

        /// <summary>
        /// AD 4
        /// </summary>
        /// <param name="table"></param>
        /// <param name="higherThen"></param>
        /// <returns></returns>
        public static IList<int> FilterElementsThigherThan(int[] table, int higherThen = 90) => table.Where(element => element > higherThen).ToList();

        /// <summary>
        /// AD 5
        /// </summary>
        /// <param name="table"></param>
        public static void MultiplyAndDisplayElements(int[] table)
        {
            var calculatedList = MultiplyElements(table);
            foreach (var (x, y, val) in calculatedList)
            {
                Console.WriteLine($"x: {x}, y: {y}, value: {val}");
            }
        }

        public static IList<Tuple<int, int, int>> MultiplyElements(int[] table)
        {
            var results = new List<Tuple<int, int, int>>();
            for (int index = 0; index < table.Length; index++)
            {
                for (int subIndex = 0; subIndex < table.Length; subIndex++)
                {
                    results.Add(new(index, subIndex, table[index] * table[subIndex]));
                }
            }
            return results.Where(item => item.Item3 > 9000).ToList();
        }

        public static void PrintSummaryOfGrowingSequences()
        {
            var table = CreateTableWithSize();

            for (int index = 0; index < table.Length - 1; index++)
            {
                table = ChangeElementsLocation(table, index);
                var (count, sequences) = CalculateCountOfGrowingSquences(table);
                var growingSequencesText = string.Join(",", sequences.Select(item => $"[{CreateTableString(item)}]"));

                Console.WriteLine($"Tablica: {index}, [{CreateTableString(table)}]");
                Console.WriteLine($"Sekwencje rosnące w T{index}: {growingSequencesText}");
                Console.WriteLine($"Liczba sekwencji {count}");

                var changedPair = index == 0 ? "BRAK" : $"{index - 1} i {index}";
                Console.WriteLine($"Zmieniona prara {changedPair}");
                Console.WriteLine();
            }
        }

        private static string CreateTableString(int[] table) => string.Join(",", table);

        public static int[] ChangeElementsLocation(int[] table, int elementToSwap)
        {
            if (elementToSwap == 0) return table;
            var result = new int[table.Length];
            table.CopyTo(result, 0);

            result[elementToSwap - 1] = table[elementToSwap];
            result[elementToSwap] = table[elementToSwap - 1];

            return result;
        }

        public static (int, IList<int[]>) CalculateCountOfGrowingSquences(int[] table)
        {
            var countOfSequences = 0;
            var sequences = new List<int[]>();
            for (int i = 1; i < table.Length - 1; i++)
            {
                if (table[i - 1] < table[i] && table[i] < table[i + 1])
                {
                    countOfSequences++;
                    sequences.Add(new[] { table[i - 1], table[i], table[i + 1] });
                }
            }
            return (countOfSequences, sequences);
        }
    }
}